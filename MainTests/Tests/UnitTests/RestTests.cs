﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Services;
using Services.Controllers;
using Services.Requests;

namespace Tests.UnitTests
{
    [TestFixture]
    public class RestTests
    {
       
        [Test]
        [Description("Positive test. Create New User")]
        public void Positive_Add()
        {
            var controller = new ImportController();

            //var id = Guid.NewGuid();
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            controller.Post(user);

            var result = Program.Users.Get(user.UserId);
            Assert.AreSame(user, result);
        }

        [Test]
        [Description("Positive test. Update User")]
        public void Positive_Update()
        {
            var controller = new ImportController();

            //Create
            var oldUser = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            controller.Post(oldUser);

            //Create new
            var updatedUser = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "EN",
               Locale = "en-US",
                DateModified = DateTime.Now.AddDays(-5)
            };
            controller.Post(updatedUser);

            //Check
            var resultUser = Program.Users.Get(updatedUser.UserId);
            Assert.AreSame(updatedUser, resultUser);

        }

        [Test]
        [Description("Negative test. Request with null 'USerInfo' parameter")]
        public void Negative_NullUserInfo()
        {
            var controller = new ImportController();
            Assert.Throws(typeof(SystemException), () => controller.Post(null), "Empty User Fault");
        }

        [Test]
        [Description("Negative test. UserId is null ")]
        public void Negative_NulllUserId()
        {
            var controller = new ImportController();
            var user = new SyncProfileRequest
            {
                //UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "EN",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "User ID field is empty");

        }

        [Test]
        [Description("Negative test. CountryIsoCode than it longet than should to be")]
        public void Negative_IsoLong()
        {
            var controller = new ImportController();
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "qwer",
               Locale = "en-US",
                DateModified = DateTime.Now
            };

            Assert.Throws(typeof(SystemException), () => controller.Post(user), "Country ISO Code has wrong format");

        }

        [Test]
        [Description("Negative test. Numbers in CountryIsoCode")]
        public void Negative_NumberIso()
        {
            var controller = new ImportController();

            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "12",
               Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "Country ISO Code has wrong format");
        }

        [Test]
        [Description("Negative test. Cyrillic symbols in CountryIsoCode")]
        public void Negative_CyrillicIso()
        {
            var controller = new ImportController();

            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "АВ",
               Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "Country ISO Code has wrong format");
        }

        [Test]
        [Description("Negative test. Empty CountryIsoCode")]
        public void Negative_EmptyIsoCode()
        {
            var controller = new ImportController();

            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "",
               Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "Country ISO Code is empty or has wrong format");

        }

        [Test]
        [Description("Negative test. CountryIsoCode = null")]
        public void Negative_Null_IsoCode()
        {
            var controller = new ImportController();
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                //CountryIsoCode = "En",
               Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "Country ISO Code is empty or has wrong format");

        }

        [Test]
        [Description("Negative test. Locale = null")]
        public void Negative_Null_Locale()
        {
            var controller = new ImportController();
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "En",
                //Locale = "Ru",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "Locale field is empty or has wrong format");
        }

        [Test]
        [Description("Negative test. Locale. Incorrect form")]
        public void Negative_Incorrect_Locale()
        {
            var controller = new ImportController();
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "En",
                Locale = "qweqwe",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "Locale ISO Code has wrong format");
        }

        [Test]
        [Description("Negative test. AdvertisingOptIn = null")]
        public void Negative_Null_AdvertisingOptInis()
        {
            var controller = new ImportController();
            
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                //AdvertisingOptIn = true,
                CountryIsoCode = "En",
               Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "AdvertisingOptIn field is empty or has wrong format");

        }

        [Test]
        [Description("Negative test. DateModified > current date")]
        public void Negative_Exceed_DateModified()
        {
            var controller = new ImportController();
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "En",
               Locale = "en-US",
                DateModified = DateTime.Now.AddDays(1)
            };
            Assert.Throws(typeof(SystemException), () => controller.Post(user), "DateModified has wrong format");
        }
    }
}
