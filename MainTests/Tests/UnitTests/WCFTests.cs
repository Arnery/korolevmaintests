﻿using System;
using System.ServiceModel;
using NUnit.Framework;
using Services;
using Services.Controllers;
using Services.Requests;
using Services.WCF;

namespace Tests.UnitTests
{
    [TestFixture]
    public class WCFTests
    {
        [Test]
        [Description("Positive test. Get user from repository")]
        public void WCF_Positive()
        {
            var controller = new ImportController();
            var service = new WCFService();
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now.AddDays(-5)
            };


            controller.Post(user);
            var userInfo = service.GetUser(user.UserId);
            var result = Utils.WСF_REST_Comparer(userInfo, user);
            Assert.True(Utils.WСF_REST_Comparer(userInfo, user), "Added user doesn't match returned user");
        }

        [Test]
        [Description("Negative test. Try to get non-existing user from repository")]
        public void WCF_Negative()
        {
            var service = new WCFService();
            var userId = Guid.NewGuid();
            Assert.Throws(typeof(FaultException<UserNotFound>),() => service.GetUser(userId), "User not found");
        }

    }
}
