﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;
using Services.Requests;
using Services.WCF;

namespace Tests
{


    [TestFixture]
    public class IntegrationTests
    {
        Process _service;

        [SetUp]
        public void SetUp()
        {
            var fileName = TestContext.CurrentContext.TestDirectory + ConfigurationManager.AppSettings["ServicesPath"];
            _service = Process.Start(new ProcessStartInfo
            {
                FileName = fileName,
                UseShellExecute = false
            });
        }

        

        [TearDown]
        public void TearDown()
        {
            _service.Kill();
        }




        [Test]
        [Description("Positive test. Adding the User")]
        public void Positive_AddUser()
        {
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Utils.ImportUser(user);
            Assert.True(Utils.WСF_REST_Comparer(Utils.ExportUser(user.UserId), user),"Can not to add the User");
        }

        [Test]
        [Description("Positive test. Updating the User")]
        public void Positive_UpdateUser()
        {
            var id = Guid.NewGuid();

            var user = new SyncProfileRequest
            {
                UserId = id,
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Utils.ImportUser(user);

            var newUser = new SyncProfileRequest
            {
                UserId = id,
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Utils.ImportUser(newUser);

            var response = Utils.ExportUser(id);
            Assert.False(Utils.WСF_REST_Comparer(response, user) && !Utils.WСF_REST_Comparer(response, newUser), "Updating Failure - no changing in DB");
            Assert.True(Utils.WСF_REST_Comparer(response, newUser), "Updating Failure - incorrect changing in DB");
        }

        [Test]
        [Description("Positive test. Add a several Users")]
        public void Positive_AddUser_Multiply()
        {

            var userOne = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            var userTwo = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                Locale = "cs-CZ",
                DateModified = DateTime.Now
            };

            var userThree = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                Locale = "en-GB",
                DateModified = DateTime.Now
            };
            var list = new List<SyncProfileRequest>{userOne, userTwo, userThree};

            foreach (var user in list) Utils.ImportUser(user);
            
            //Note: it should to be a two cycles. We should to work with full database
            foreach (var user in list) Assert.True(Utils.WСF_REST_Comparer(Utils.ExportUser(user.UserId), user), $"Some user hadn't been added to DB:{user.UserId}");
        }

        [Test]
        [Description("Positive test. Update a several Users")]
        public void Positive_UpdateUser_Multiply()
        {
            var userOne = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            var userTwo = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                Locale = "cs-CZ",
                DateModified = DateTime.Now
            };

            var userThree = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                Locale = "en-GB",
                DateModified = DateTime.Now
            };
            var list = new List<SyncProfileRequest> { userOne, userTwo, userThree };

            foreach (var user in list) Utils.ImportUser(user);
            //Note: it should to be a two cycles. We should to work with full database
            var inputUser = new SyncProfileRequest
            {
                UserId = userThree.UserId,
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            
            Utils.ImportUser(inputUser);
            ;
            Assert.True(Utils.WСF_REST_Comparer(Utils.ExportUser(userThree.UserId), inputUser), "Multiply adding failure");
            //foreach (var user in list) Assert.True(Utils.WСF_REST_Comparer(Utils.ExportUser(user.UserId), user), "Some user hadn't been added to DB");
      
        }


        [Test]
        [Description("Negative test. User not exist in DB")]
        public void Negative_User_NotExist()
        {
            var userId = Guid.NewGuid();
            try
            {
                Utils.ExportUser(userId);
            }
            catch (Exception e)
            {
                Assert.True(e.Message.Contains("User not found in DB. UserId:"));
            }
        }

        [Test]
        [Description("Negative test. UserInfo = null")]
        public void Negative_User_Null()
        {
            Assert.True(Utils.ImportUser(null).Contains("Updating failure. Empty User"), "Null User not found exception to be thrown");
        }

        [Test]
        [Description("Negative test. UserID = null")]
        public void Negative_Null_UserId()
        {
            var user = new SyncProfileRequest
            {
                //UserId = id,
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("UserId field is empty"),"Null User ID checking fail");
        }


        [Test]
        [Description("Negative test. UserID = Empty GUID")]
        public void Negative_Empty_UserId()
        {
            var user = new SyncProfileRequest
            {
                UserId = Guid.Empty,
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("UserId field is empty"), "Empty User ID checking fail");
        }


        [Test]
        [Description("Negative test. CountryIsoCode = Null")]
        public void Negative_Null_ISOCode()
        {

            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                //CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("CountryIsoCode field is empty"), "Null Country ISO Code checking fail");
        }

        [Test]
        [Description("Negative test. CountryIsoCode = Empty")]
        public void Negative_Empty_ISOCode()
        {
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("Country ISO Code has wrong format"), "Country ISO Code format checking fail");
        }

        [Test]
        [Description("Negative test. Incorrect format of CountryIsoCode")]
        public void Negative_Incorrect_ISOCode()
        {

            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "asd",
                Locale = "en-US",
                DateModified = DateTime.Now
            };

            string response = Utils.ImportUser(user);
            Console.WriteLine(response);
            if (!response.Contains("Country ISO Code has wrong format"))Assert.Fail("Country ISO Code format checking fail");
        }

        [Test]
        [Description("Negative test. Cyrillic symbols in CountryIsoCode")]
        public void Negative_Cyrillic_ISOCode()
        {

            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "ЕН",
                Locale = "en-US",
                DateModified = DateTime.Now
            };

            string response = Utils.ImportUser(user);
            Console.WriteLine(response);
            if (!response.Contains("Country ISO Code has wrong format")) Assert.Fail("Country ISO Code format checking fail");
        }

        [Test]
        [Description("Negative test. Numbers in CountryIsoCode")]
        public void Negative_Numeral_ISOCode()
        {
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "12",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("Country ISO Code has wrong format"), "Country ISO Code format checking fail");
        }

        [Test]
        [Description("Negative test. Locale = null")]
        public void Negative_Null_Locale()
        {
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                //Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("Locale field is empty"), "Locale checking fail");
        }


        [Test]
        [Description("Negative test. Locale incorrect format")]
        public void Negative_Incorrect_Locale()
        {
            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                AdvertisingOptIn = true,
                CountryIsoCode = "En",
                Locale = "qweqwe",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("Locale ISO Code has wrong format"), "Locale format checking fail");
        }

        [Test]
        [Description("Negative test. AdvertisingOptIn = null")]
        public void PostTest_AdvertisingOptInIsEmpty_viaRequest()
        {

            var user = new SyncProfileRequest
            {
                UserId = Guid.NewGuid(),
                RequestId = Guid.NewGuid(),
                //AdvertisingOptIn = true,
                CountryIsoCode = "Ru",
                Locale = "en-US",
                DateModified = DateTime.Now
            };
            Assert.True(Utils.ImportUser(user).Contains("AdvertisingOptIn field is empty"), "AdvertisingOptIn checking fail");
        }
    }
}
