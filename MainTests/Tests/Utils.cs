﻿using System;
using System.Configuration;
using System.Net.Http;
using System.ServiceModel;
using NUnit.Framework;
using Services.Requests;
using Services.WCF;

namespace Tests
{

    public static class Utils
    {
        public static bool WСF_REST_Comparer(UserInfo userInfo, SyncProfileRequest user)
        {
            return user.UserId == userInfo.UserId &&
                   user.AdvertisingOptIn == userInfo.AdvertisingOptIn &&
                   user.CountryIsoCode == userInfo.CountryIsoCode &&
                   user.DateModified == userInfo.DateModified &&
                   user.Locale == userInfo.Locale;
        }

        /// <summary>
        /// Rest Request for Importr User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string ImportUser(SyncProfileRequest user)
        {
            try
            {
                var url = $"{ConfigurationManager.AppSettings["Host"]}:{(ConfigurationManager.AppSettings["WebPort"])}/import";
                var client = new HttpClient();
                var result = client.PostAsJsonAsync(url, user).GetAwaiter().GetResult();
                return result.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                Console.WriteLine($"User import failure:{e}");
                Assert.Fail($"User import failure:{e}");
                throw;
            }


        }

        /// <summary>
        /// Wvf Request for export user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static UserInfo ExportUser(Guid userId)
        {
            var binding = new BasicHttpBinding();
            var endpoint = new EndpointAddress($"{ConfigurationManager.AppSettings["Host"]}:{ConfigurationManager.AppSettings["WcfPort"]}/");
            var channelFactory = new ChannelFactory<IUserInfoProvider>(binding, endpoint);
            IUserInfoProvider client = null;
            try
            {
                client = channelFactory.CreateChannel();
                var user = client.GetUser(userId);
                (client as ICommunicationObject).Close();
                return user;
            }
            catch (Exception)
            {
                (client as ICommunicationObject)?.Abort();
                throw;
            }
        }
    }
}
