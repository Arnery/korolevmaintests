﻿using System;
using System.Collections.Concurrent;
using Services.Requests;

namespace Services
{
     public class Repository
     {
        private ConcurrentDictionary<Guid, SyncProfileRequest> users = new ConcurrentDictionary<Guid, SyncProfileRequest>();

        public bool Add(SyncProfileRequest user)
        {
            return users.TryAdd(user.UserId, user);
        }

        public bool Update(SyncProfileRequest user)
        {
            var result = users.TryGetValue(user.UserId, out var oldUser);
            return users.TryUpdate(user.UserId, user, oldUser);
        }

        public bool Exist(Guid userId)
        {
            return users.ContainsKey(userId);
        }

        public SyncProfileRequest Get(Guid userId)
        {
            return users.TryGetValue(userId, out var user) ? user : null;
        }

    }
}
