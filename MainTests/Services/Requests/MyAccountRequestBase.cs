﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Services.Requests
{

    public abstract class MyAccountRequestBase
    {
        private Guid requestId { get; set; }

        [Required]
        public Guid RequestId
        {
            get => requestId.Equals(Guid.Empty) ? Guid.NewGuid() : requestId;
            set => requestId = value;
        }

        [Key]
        [Required]
        public Guid UserId { get; set; }
    }

}
