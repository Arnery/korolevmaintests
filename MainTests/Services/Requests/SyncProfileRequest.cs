﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Services.Requests
{
    public class SyncProfileRequest : MyAccountRequestBase
    {
        public bool? AdvertisingOptIn { get; set; }

        [Required]
        public string CountryIsoCode { get; set; }

        private DateTime dateModified { get; set; }

        public DateTime DateModified
        {
            get { return dateModified.Equals(DateTime.MinValue) ? DateTime.Now : dateModified; }
            set { dateModified = value; }
        }

        public string Locale { get; set; }
    }
}
