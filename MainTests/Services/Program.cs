﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceModel;
using Microsoft.Owin.Hosting;
using Serilog;
using Services.WCF;

namespace Services
{
    //Windows defender can to find "bearfoos.a!ml" troyan. You need to accept permission in WD window to detected dunger
    // https://www.microsoft.com/en-us/wdsi/threats/malware-encyclopedia-description?Name=Program:Win32/Bearfoos.A!ml&ThreatID=247604
    public class Program
    {
        public static Repository Users = new Repository();
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.LiterateConsole()
                .WriteTo.RollingFile("logs\\{Date}.txt")
                .CreateLogger();
            
            var url = $"{ConfigurationManager.AppSettings["Host"]}:{ConfigurationManager.AppSettings["WebPort"]}";

            try
            {
                using (ServiceHost host = new ServiceHost(typeof(WCFService)))
                {
                    host.Open();
                    Log.Information("WCF Service launched");
                    using (WebApp.Start<StartUp>(url))
                    {

                        Log.Information("REST Service launched\nPress enter to close the services");
                        Console.ReadLine();
                    }
                }
            }
            catch (FaultException<UserNotFound> userNotFoundException)
            {
                Log.Error(userNotFoundException, "User not founded");
                Console.WriteLine(userNotFoundException.Detail.Message);
            }
        }
    }
}

