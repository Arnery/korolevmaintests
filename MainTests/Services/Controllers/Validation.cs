﻿using System;
using System.Globalization;
using System.Linq;
using Serilog;

namespace Services.Controllers
{
    /// <summary>
    /// Field Validation class
    /// </summary>
    public static class Validation
    {
        public static void NullChecking(object obj, string name)
        {
            if (obj != null) return;
            Log.Error(name + " empty. Can't add the user to the database");
            throw new SystemException(name + " field is empty");
        }

        public static void GuidValidation(Guid guid, string name)
        {
            if (guid != Guid.Empty) return;
            Log.Error(name + " field is empty. Can't add the user to the database");
            throw new SystemException(name + " field is empty");
        }

        public static void IsoCodeValidation(string countryIsoCode)
        {
            if (countryIsoCode.Length > 2 || !CultureInfo.GetCultures(CultureTypes.AllCultures)
                    .Any(x => x.TwoLetterISOLanguageName.ToLower().Equals(countryIsoCode.ToLower())))
            {
                Log.Error("Country ISO Code format checking failure. Can't add the user to the database");
                throw new SystemException("Country ISO Code has wrong format");
            }
        }

        
        public static void LocaleValidation(string localeIsoCode)
        {
            if (CultureInfo.GetCultures(CultureTypes.AllCultures)
                .Any(x => x.TextInfo.CultureName.Equals(localeIsoCode))) return;
            Log.Error("Locale ISO Code has wrong format. Can't add the user to the database");
            throw new SystemException("Locale ISO Code has wrong format");
        }

        public static void DateValidation(DateTime date)
        {
            if (date <= DateTime.Now) return;
            Log.Error("DateModified has wrong format. Can't add the user to the database");
            throw new SystemException("DateModified has wrong format");

        }

    }
}
