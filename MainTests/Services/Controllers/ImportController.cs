﻿using System;
using System.Web.Http;
using Serilog;
using Services.Requests;

namespace Services.Controllers
{

    [RoutePrefix("")]
    public class ImportController : ApiController
    {
        //[Route("~/import.json")]
        [Route("~/import")]
        public void Post([FromBody]SyncProfileRequest User)
        {
            //validation
            if (User == null)
            {
                Log.Error("Empty request received");
                throw new SystemException("Updating failure. Empty User");
            }
            // RequestId and DateModified cannot be null. Its value forming automaticall in case of null
            
            Validation.NullChecking(User.UserId, "UserId");
            Validation.NullChecking(User.AdvertisingOptIn, "AdvertisingOptIn");
            Validation.NullChecking(User.CountryIsoCode, "CountryIsoCode");
            Validation.NullChecking(User.Locale, "Locale");

            Validation.DateValidation(User.DateModified);

            Validation.GuidValidation(User.UserId, "UserId");
            Validation.GuidValidation(User.RequestId, "RequestId");

            Validation.IsoCodeValidation(User.CountryIsoCode);
            Validation.LocaleValidation(User.Locale);

          
            if (Program.Users.Exist(User.UserId))
            {
                Log.Information($"Updating user ID:{User.UserId}");
                Program.Users.Update(User);
            }
            else
            {
                Program.Users.Add(User);
                Log.Information($"Adding user ID:{User.UserId}");
            }
            
        }
    }
}
