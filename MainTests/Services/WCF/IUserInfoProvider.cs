﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Services.WCF
{
    [ServiceContract]
    public interface IUserInfoProvider
    {
        [OperationContract]
        [WebGet]
        [FaultContract(typeof(UserNotFound))]
        UserInfo GetUser(Guid userId);
    }
}
