﻿using System;
using System.ServiceModel;
using Serilog;
using Services.Requests;

namespace Services.WCF
{
    
    public class WCFService : IUserInfoProvider
    {
        public UserInfo GetUser(Guid userId)
        {
            var user = Program.Users.Get(userId);
            Log.Information($"Try to find UserId:{userId}");
            if (user != null)
            {
                Log.Information($"UserId:{userId} is found");
                return new UserInfo
                {
                    UserId = user.UserId,
                    AdvertisingOptIn = user.AdvertisingOptIn,
                    CountryIsoCode = user.CountryIsoCode,
                    DateModified = user.DateModified,
                    Locale = user.Locale
                };
            }
            Log.Information($"UserId = {userId} not found in DB");
            throw new FaultException<UserNotFound>(new UserNotFound("User not found"), $"User not found in DB. UserId:{userId}");

        }
    }



}


