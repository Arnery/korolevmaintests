﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Services.WCF
{
    [DataContract]
    public class UserNotFound
    {
        public UserNotFound(string errorMessage)
        {
            Message = errorMessage;
        }
        [DataMember]
        public string Message { get; set; }

    }
}
